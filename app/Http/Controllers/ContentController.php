<?php

namespace fitness\Http\Controllers;

use fitness\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents =  Content::all();

        $images =  Content::where('type','image')->get();
        $text =  Content::where('type','text')->get();
     
        return view('Dashboard.variables.index', compact('images', 'text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \fitness\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \fitness\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \fitness\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $contents =  Content::all();
        $default =  Content::where('name','Home/Default')->first();

   

        $item = $request['content'];
        
        if ($request->get('default') === '1'){
                
            $item = $default->content;
          
       
         
        }
        else{

            $item = $request['content'];

        }
       
        $content = Content::find($id);
        $content->content = $item;
        $content->save();

        return redirect()->back()->with('success','The '.  $content->name  . ' '.  $content->type  .'  has been updated');
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \fitness\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        //
    }
}
