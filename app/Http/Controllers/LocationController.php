<?php

namespace fitness\Http\Controllers;

use fitness\Location;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations =  DB::table('locations')->orderBy('location', 'asc')
        ->get();
        return view('Dashboard.locations.index', compact('locations'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashboard.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  $request->validate([

            ]);
            $validator = Validator::make($request->all(), [
                'location' => 'required',

            ]);


            if ($validator->fails()){
                \Session::flash('Warning', 'Please enter the valid details');
                return Redirect::to('/category')->withInput()->withErrors($validator);
            }

            $location = new Location;
            $location->location = $request['location'];
             $location->save();

            return redirect('dashboard/locations')->with('success',  $location->location . ' has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $locations =  Location::all();
        return view('Dashboard.locations.index', compact('locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locations = Location::find($id);
        return view('Dashboard.locations.edit',compact('locations','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::find($id);
        $location->location=$request->get('location');
        $location->save();
        return redirect('dashboard/locations')->with('success',   $location->location  . ' has been updated');
        
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Location::destroy($id);

        return redirect('dashboard/locations')->with('success', 'Location deleted!');
    }
}
