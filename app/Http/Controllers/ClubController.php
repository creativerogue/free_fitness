<?php

namespace fitness\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;

use fitness\Club;
use fitness\Category;
use fitness\Location;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
       $clubs=  Club::all();


        return view('Dashboard.club.index', compact('clubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories =  Category::all();
        $locations =  Location::all();
        return view('Dashboard.club.create', compact('categories','locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $validator =  $request->validate([

            ]);
            $validator = Validator::make($request->all(), [
             'category' => 'required',
			'name' => 'required',
			'description' => 'required',
			'logo' => 'required'
            ]);


            if ($validator->fails()){
                \Session::flash('Warning', 'Please enter the valid details');
                return Redirect::to('/dashboard/clubs')->withInput()->withErrors($validator);
            }

            $club = new Club;
            $club->category = $request['category'];
            $club->name = $request['name'];
            $club->location = $request['location'];
            $club->description = $request['description'];
            $club->logo = $request['logo'];

            $club->save();

            \Session::flash('success', 'Club added successfully');


        return redirect('/dashboard/clubs')->with('success', 'Club added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $club = Club::findOrFail($id);

        return view('Dashboard.club.index', compact('club'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $club = Club::find($id);
        $locations =  Location::all();
        $categories =  Category::all();
        $activities = Category::groupBy('category')->pluck('category','category');

        return view('Dashboard.club.edit', compact('club' , 'activities', 'categories','id','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

        public function update(Request $request, $id)
        {
            $locations =  Location::all();
            $categories =  Category::all();
            $activities = Category::groupBy('category')->pluck('category','category');

            $club = Club::find($id);
            $club->location = $request['location'];
            $club->category = $request['category'];
            $club->name = $request['name'];
            $club->description = $request['description'];
            $club->logo = $request['logo'];

            $club->save();

               return view('Dashboard.club.edit', compact('club' , 'activities', 'categories','id','locations'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Club::destroy($id);

        return redirect('/dashboard/clubs')->with('flash_message', 'Club deleted!');
    }
}
