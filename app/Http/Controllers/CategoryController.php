<?php

namespace fitness\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use fitness\Category;

class CategoryController extends Controller
{

    public function show()

    {
        $categories =  Category::all();
        return view('Dashboard.category.index', compact('categories'));

     }

    public function index()

    {
        $categories =  Category::all();
        return view('Dashboard.category.index', compact('categories'));

     }

     public function create()
     {
         return view('Dashboard.category.create');
     }

    public function store(Request $request)
  {
    $validator =  $request->validate([

        ]);
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'colour' => 'required',
        ]);


        if ($validator->fails()){
            \Session::flash('Warning', 'Please enter the valid details');
            return Redirect::to('/category')->withInput()->withErrors($validator);
        }

        $category = new Category;
        $category->category = $request['category'];
        $category->colour = $request['colour'];

        $category->save();

        \Session::flash('Success', 'Category added successfully');
       
        return redirect('dashboard/category')->with('success',  $category->category  . ' has been created');
  }
 /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('Dashboard.category.edit',compact('category','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->category=$request->get('category');
        $category->colour=$request->get('colour');
        $category->save();
        return redirect('dashboard/category')->with('success',  $category->category  . ' has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Category::destroy($id);

        return redirect('dashboard/category')->with('success', 'Activity deleted!');
     
    }

}
