<?php

namespace fitness\Http\Controllers;

use fitness\Article;
use fitness\Category;
use fitness\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles =  Article::all();
        return view('Dashboard.blog.index', compact('articles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories =  Category::all();
        return view('Dashboard.blog.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => [
                'min:10',
                'max:200',
                
            ],
            [
                'title.min' => 'The title must contain more than ten characters',
                'title.max' => 'The title must contain less than two hundred characters',
           
            ]]);

        $article = new Article;
        $article->title=$request->get('title');
        $article->body=$request->get('body');
        $article->image=$request->get('image');
        $article->category=$request->get('category');
        $article->author = Auth::user()->name;
        $article->save();

         
        return redirect('dashboard/articles')->with('success',  $article->title  . ' has been created');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $articles =  Article::all();
        return view('Dashboard.blog.index', compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $users = User::all();
        $categories =  Category::where('category','<>',$article->category)->get();
        
        return view('Dashboard.blog.edit',compact('article','id', 'categories', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \fitness\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'title' => [
                'min:10',
                'max:200',
                
            ],
            [
                'title.min' => 'The title must contain more than ten characters',
                'title.max' => 'The title must contain less than two hundred characters',
           
            ]]);
            
        $article = Article::find($id);
        $article->title=$request->get('title');
        $article->body=$request->get('body');
        $article->image=$request->get('image');
        $article->category=$request->get('category');
        $article->author = $request->get('author');
        $article->save();
       
     
        return redirect()->back()->with('success',  $article->title  . ' has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Article::destroy($id);

        return redirect('dashboard/articles')->with('success', 'article deleted!');
    }
}
