<?php

namespace fitness\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Calendar;
use Validator;
use fitness\Event;
use fitness\Club;
use fitness\Article;
use fitness\Location;
use fitness\Category;
use fitness\Content;
use Carbon\Carbon;
use Vinkla\Instagram\Instagram;


class PagesController extends Controller
{

    public function home()
    {


        $content =  Content::where('name','Home/Default')->first();
        $header = $content->content;
        $hText = Content::where('name','Home Header')->first();
        
        $headertext = $hText->content;

        $events = "No events";
        if (Session::has('result')){
            $result = Session::get('result');

          // dd($result);
           
            $events = Event::whereIn('club_id', $result)->get();
    
         
          }
          else {
            $events = Event::all();
    
          };
        $event_list = [];
        foreach ($events as $key =>$event){

        $start_date = $event->startDate." T ".$event->startTime;
        $end_date = $event->endDate." T ".$event->endTime;

        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);

        $name = $event->event_name;
        $club = Event::find($event->id)->club->category;
        $club_id = Event::find($event->id)->club->id;
        
        $location = Event::find($event->id)->club->location;
        $colour =  Event::find($event->id)->club->colour['colour'];

        $event_name =  $club." - ".$event->event_name." - ".$location;

        if ($event->all_day == 1){
        $all_day = true;
    }
        else{
            $all_day = false;

        }

        switch ($event->frequency):
        case 0:

        $start_date = $start_date;
        $event_list [] = Calendar::event(
        $event_name,
        $all_day,
        $start_date,
        $end_date,
        $event->id,
        [
           
            'url' => '/club/'.$club_id.'/'.str_slug($name),
          'backgroundColor'  => $colour,

        ]);
        break;

        case 1:
        for( $i = 0; $i <= 52; $i++ ){
        $start_date = $start_date->addWeek();
        $event_list [] = Calendar::event(
        $event_name,
        $all_day,
        new \DateTime($start_date.' -1 week'),
        new \DateTime($end_date),
        $event->id,
        [
            'url' => '/club/'.$club_id.'/'.str_slug($name),
            'backgroundColor'  => $colour,

        ]);
         }

        break;
        case 2:
        for( $i = 0; $i <= 52; $i++ ){
        $start_date = $start_date->addWeek(2);
        $event_list [] = Calendar::event(
        $event_name,
        $all_day,
        new \DateTime($start_date.' -2 weeks'),
        new \DateTime($end_date),
        $event->id,
         [
            'url' => '/club/'.$club_id.'/'.str_slug($name),
            'backgroundColor'  => $colour,

          ] );}

        break;
        case 4:
        for( $i = 0; $i <= 52; $i++ ){
        $start_date = $start_date->addWeek(4);
        $event_list [] = Calendar::event(
        $event_name,
        $all_day,
        new \DateTime($start_date.' -4 weeks'),
        new \DateTime($end_date),
        $event->id,
          [
            'url' => '/club/'.$club_id.'/'.str_slug($name),
            'backgroundColor'  => $colour,

           ] ); }
        break;
        endswitch;
         };


         $calendar_details = Calendar::addEvents($event_list)->setOptions(
             [
                 'timeFormat' =>'h:mm a',
                 'defaultView' => 'agendaWeek',
                 'duration' => '05:00:00',
                 
        ]);

        $c1 =  Club::count();
        $c2 = Event::count();
        $c3 = Category::count();
        $c4 = Club::groupBy('location')->pluck('location','location')->count();
        $clubs =  Club::all();
        $locations =  Location::all();
        $activities =  Category::all();


        $instagram = new Instagram('5447051238.1677ed0.dd53fc6a36594cf490b01c691aa5a8b2');
        $instafeed =  $instagram->media();



        return view('Frontend.index', compact ('calendar_details', 'header', 'headertext',  'footerText' ,'c1', 'c2', 'c3', 'c4', 'locations', 'clubs', 'activities', 'instafeed') );
     }

     public function about()
     {
        $content =  Content::where('name','About')->first();
        $header = $content->content;

        $TB1 = Content::where('name','About block 1')->first();
       
        $TB1  = $TB1->content;
      
        $TB2 = Content::where('name','About block 2')->first();
   
        $TB2  = $TB2->content;

         $clubs =  Club::all();
         $activities = Club::groupBy('category')->pluck('category','category');


         return view('Frontend.about', compact('clubs' , 'activities', 'header' ,'TB1', 'TB2'));
      }


     public function clubs()
     {

        $content =  Content::where('name','Clubs')->first();
        $header = $content->content;

         $clubs =  Club::all();
         $activities = Club::groupBy('category')->pluck('category','category');


         return view('Frontend.clubs', compact('clubs' , 'activities', 'header'));
      }


     public function club($id)
     {
        $content =  Content::where('name','Clubs')->first();
        $header = $content->content;

        $club = Club::find($id);
        $events = Event::Where ('club_id', $id)->get();


         return view('Frontend.club', compact('club', 'events', 'header'));
      }
      public function contact()
      {

          $clubs =  Club::all();
          $activities = Club::groupBy('category')->pluck('category','category');


          return view('Frontend.contact', compact('clubs' , 'activities'));
       }


      
       public function  blogLanding()
       {

        $content =  Content::where('name','Culture')->first();
        $header = $content->content;

         $articles =  Article::all();
         $category= NULL;
         return view('Frontend.blog.landing', compact('articles', 'category', 'header' ));
        }

        public function  blogItem($id)
        {
           
            $content =  Content::where('name','Culture')->first();
            $header = $content->content;
            
            $article = Article::find($id);
            $articles =  Article::where('id','<>',$id)->get();
            $categories = Article::groupBy('category')->pluck('category','category');
      
            return view('Frontend.blog.blog', compact('article' ,'articles' , 'categories', 'header'));


         }

         
        public function  blogcatagory($category)
        {
                      
            
            $articles =  Article::where('category',$category)->get();
          
            return view('Frontend.blog.landing', compact('articles', 'category'  ));
      
        


         }


  

}
