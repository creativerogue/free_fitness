<?php

namespace fitness\Http\Controllers;

use Illuminate\Http\Request;
use fitness\Event;
use fitness\Club;
use fitness\Location;
use fitness\Category;

class SearchController extends Controller
{
   /**

     * Search for a user by department, role and location

     * If any are empty, they will be ignored.

     * Uses Conditional Clause: when()

     * See: https://laravel.com/docs/5.6/queries#conditional-clauses

     *

     * @param Request $request

     * @return void

     */

    public function Search(Request $request)

    {
      $club = $request->get('club');

      if($club !=='0'){
       
        $result = Event::when($club, function ($query) use ($club) { return $query->where('club_id', $club);})->pluck('club_id');
       }
           else{
      
        $activity = $request->get('activity');
        $location = $request->get('location');
        
        $result = Club::when($activity, function ($query) use ($activity) {
            return $query->where('category', $activity);
        })
        ->when($location, function ($query) use ($location) {
            return $query->where('location', $location);
        })
        ->pluck('id');
 }




      return redirect('/')->with('result',$result);
    }
}
