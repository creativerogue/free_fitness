<?php

namespace fitness\Http\Controllers;


use Request;
use fitness\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
class UserController extends Controller
{

    public function show()

    {
        $users =  User::all();
        return view('Dashboard.users.index', compact('users'));

     }

    public function index()

    {
        $users =  User::all();
        return view('Dashboard.users.index', compact('users'));

     }

     public function create()
     {
         return view('Dashboard.users.create');
     }
    public function store()
    {
        // get the form data for the user
        $userFormData = Request::all();


        // write the validation rules for your form
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',

        ];
        // validate the user form data
        $validation = Validator::make($userFormData, $rules);

        // if validation fails
        if($validation->fails())
        {
            // redirect back to the form
            return redirect()->back();
        }


        // if validation passes
        // save the user to the database


        User::create([
            'name' => $userFormData['name'],
            'email' => $userFormData['email'],
            'password' => Hash::make($userFormData['password']),
        ]);
        // return a view
        \Session::flash('Success', 'User added successfully');
        return Redirect::to('dashboard/users');

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function destroy($id)
    {
        User::destroy($id);

        return redirect('/dashboard/users')->with('flash_message', 'User deleted!');
    }
}
