<?php

namespace fitness\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Calendar;
use Validator;
use fitness\Event;
use fitness\Club;
use fitness\Category;
use Carbon\Carbon;

class EventController extends Controller
{
    public function show()

    {

          return view ('Dashboard.events.index');


     }

   public function index(){

    $events = "No events";
    $events = Event::get();

    $event_list = [];
    foreach ($events as $key =>$event){



    $start_date = $event->startDate." T ".$event->startTime;
    $end_date = $event->endDate." T ".$event->endTime;

    $start_date = Carbon::parse($start_date);
    $end_date = Carbon::parse($end_date);


    $club = Event::find($event->id)->club->category;
    $club_name = Event::find($event->id)->club->name;

    $colour =  Event::find($event->id)->club->colour['colour'];

    $event_name =  $club." - ".$event->event_name;

    if ($event->all_day == 1){
    $all_day = true;
}
    else{
        $all_day = false;

    }

    switch ($event->frequency):
    case 0:

    $start_date = $start_date;
    $event_list [] = Calendar::event(
    $event_name,
    $all_day,
    $start_date,
    $end_date,
    $event->id,
    [
      'url' => '/dashboard/events/'.$event->id.'/edit',
      'backgroundColor'  => $colour,

    ]);
    break;

    case 1:
    for( $i = 0; $i <= 52; $i++ ){
    $start_date = $start_date->addWeek();
    $event_list [] = Calendar::event(
    $event_name,
    $all_day,
    new \DateTime($start_date.' -1 week'),
    new \DateTime($end_date),
    $event->id,
    [
        'url' => '/dashboard/events/'.$event->id.'/edit',
        'backgroundColor'  => $colour,

    ]);
     }

    break;
    case 2:
    for( $i = 0; $i <= 52; $i++ ){
    $start_date = $start_date->addWeek(2);
    $event_list [] = Calendar::event(
    $event_name,
    $all_day,
    new \DateTime($start_date.' -2 weeks'),
    new \DateTime($end_date),
    $event->id,
     [
        'url' => '/dashboard/events/'.$event->id.'/edit',
        'backgroundColor'  => $colour,

      ] );}

    break;
    case 4:
    for( $i = 0; $i <= 52; $i++ ){
    $start_date = $start_date->addWeek(4);
    $event_list [] = Calendar::event(
    $event_name,
    $all_day,
    new \DateTime($start_date.' -4 weeks'),
    new \DateTime($end_date),
    $event->id,
      [
        'url' => '/dashboard/events/'.$event->id.'/edit',
        'backgroundColor'  => $colour,

       ] ); }
    break;
    endswitch;
     };


     $calendar_details = Calendar::addEvents($event_list)->setOptions(
         [	'defaultView' => 'agendaWeek',
             'timeFormat' =>'h:mm a',
    ]);


      return view ('Dashboard.events.index', compact ('calendar_details', 'events') );
     }

     public function create()
     {
        $clubs =  Club::all();

         return view('Dashboard.events.create',compact('clubs'));
     }

    public function store(Request $request)
  {
    $validator =  $request->validate([

        ]);


    $event = new Event;
    $event->club_id = $request['club_id'];
    $event->event_name = $request['event_name'];
    $event->event_location = $request['event_location'];
    $event->event_day = $request['event_day'];
    $event->startDate = $request['startDate'];
    $event->endDate = $request['startDate'];
    $event->startTime = $request['startTime'];
    $event->endTime = $request['endTime'];
    $event->all_day = $request['all_day'];
    $event->reccuring = $request['reccuring'];
    $event->frequency = $request['frequency'];
    $event->save();


    return redirect('/dashboard/events')->with('success', 'Event added successfully');
  }




/**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
       $event = Event::find($id);


       $clubs =  Club::all();

       $club  = Club::where('id', $event->club_id)->get();


       //$categories =  Category::all();
       //$activities = Category::groupBy('category')->pluck('category','category');
      return view('Dashboard.events.edit',compact('event','id', 'clubs', 'club'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {



      $event = Event::find($id);
      $event->club_id = $request['club_id'];
      $event->event_name = $request['event_name'];
      $event->event_location = $request['event_location'];
      $event->event_day = $request['event_day'];
      $event->startDate = $request['startDate'];
      $event->endDate = $request['startDate'];
      $event->startTime = $request['startTime'];
      $event->endTime = $request['endTime'];
      $event->all_day = $request['all_day'];
      $event->reccuring = $request['reccuring'];
      $event->frequency = $request['frequency'];
      $event->save();


      return redirect('/dashboard/events')->with('success', 'Event updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function destroy($id)
  {
    Event::destroy($id);

    return redirect('/dashboard/events')->with('success', 'Event deleted!');
    
  }

}
