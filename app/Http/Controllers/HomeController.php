<?php

namespace fitness\Http\Controllers;

use Illuminate\Http\Request;
use fitness\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories =  Category::all();

        return view('Dashboard.index', compact('categories'));

    }
}
