<?php

namespace fitness\Providers;
use View;
use fitness\Content;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider


{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
      
        view()->composer('layouts.frontend', function ($view) {
            //
           
          $text  =  Content::where('name','Home Footer')->first();
          $footer = $text->content;
          $view->with('footertext',$footer);
  
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
