<?php

namespace fitness;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'EventName',
        'startDate',
        'endDate'
    ];

    public function club()
    {
        return $this->hasOne('fitness\Club', 'id', 'club_id');
    }
}
