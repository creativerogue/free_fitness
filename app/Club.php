<?php

namespace fitness;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clubs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Activity', 'Community', 'About', 'Activity_image', 'Activity_link', 'Activity_content'];

    public function events()
    {
        return $this->hasMany('fitness\Events');
    }

    public function colour()
    {
        return $this->belongsTo('fitness\Category', 'category', 'category');

    }

}
