<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119383522-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119383522-1');
</script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Fitness First London') }}</title>

      <!-- Styles -->

      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">

<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/favicons/favicon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="256x256" href="{{ asset('images/favicons/favicon-256x256.png') }}">
<link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="#06aaff">
<link rel="manifest" href="{{ asset('images/favicons/site.webmanifest') }}">



<meta name="msapplication-config" content="{{ asset('images/favicons/browserconfig.xml') }}">

       <!-- Fonts -->
       <link rel="dns-prefetch" href="https://fonts.gstatic.com">
       <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
       <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

       <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/lfm.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">

       <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

       <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/stroke-icon/style.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/flat-icon/flaticon.css') }}">

        <!-- Extra plugin css -->
       <link rel="stylesheet" href="{{ asset('vendor/magnify-popup/magnific-popup.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/owl-carousel/owl.carousel.min.css') }}">
       <link rel="stylesheet" href="{{ asset('vendor/lightbox/simpleLightbox.css') }}">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <!-- Scripts -->


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

 <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bd99fdc8a1fb80011151c7a&product=social-ab' async='async'></script>



</head>
  <body>


          <!--================Header Area =================-->
          <header class="main_header_area">
            <div class="header_top">
                <div class="container">
                    <div class="header_top_inner d-sm-flex justify-content-sm-between">
                        <div class="left_info">

                            <a href="mailto:info@freefitness.london"><i class="fa fa-envelope-o"></i>info@freefitness.london</a>
                        </div>
                        <ul class="header_social ">
                            <li><a href="https://www.instagram.com/freefitness.london" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://twitter.com/freefitnessLDN " target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/freefitnesslondon/ " target="_blank"><i class="fa fa-facebook-f"></i></a></li>


                            <!-- Authentication Links -->
                            @guest


                            @else
                                <li>

                                    <a  href="{{ route('dashboard') }}" > Dashboard</a>


                                        <a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header_menu">
                <nav class="navbar navbar-expand-lg">
                   <div class="container">
                        <a class="navbar-brand" href="{{route('home')}}"><img src="{{ asset('images/logo.png') }}" alt=""><img src="{{ asset('images/top-sticky-bar-logo.png') }}" alt=""></a>
                        <!-- Small Divice Menu-->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_supported" aria-controls="navbar_supported" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse justify-content-end" id="navbar_supported">
                            <ul class="navbar-nav">

                                        <li><a href="{{route('culture')}}">Fitness Culture</a></li>
                                        <li><a href="{{route('about')}}">About Us</a></li>
                                         <li><a class="nav-link end_menu" href="{{route('clubs')}}">Clubs</a></li>

                            </ul>
                        </div>
                   </div>
                </nav>
            </div>
        </header>
        <!--================Header Area =================-->

        <!--================Banner Area =================-->



            @yield('content')


        <!--================Footer Area =================-->
        <footer class="footer_area">
                <div class="container">


                 <div class="row m-0 footer_subscriber">
                 <div class="form-group col-lg-9 mb-3">
                    <div class="find_out_text text-center"><h2>Notify me about Free Fitness London news and activities</h2></div>
                  </div>

                         <div class="form-group col-lg-2 ">
                        <button class="find_btn btn" type="submit"  data-toggle="modal" data-target="#newsletter">Subscribe</button>
                        </div>

                    </div>


                    <div class="row footer_row">
                        <div class="col-lg-6 col-md-6 fooer_logo">
                            <a href="/"><img src="{{ asset('images/logo.png') }}" alt=""></a>
                                 {!!$footertext!!}
                            <ul class="social_icon">
                                 <li><a href="https://www.instagram.com/freefitness.london" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://twitter.com/freefitnessLDN " target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/freefitnesslondon/ " target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 quick">

                            <ul class="quick_links">
                              <li><a href="{{route('culture')}}">- Fitness Culture</a></li>

                                <li><a href="{{route('about')}}">-  About us</a></li>
                                <li><a href="{{route('clubs')}}">-  Clubs</a></li>

                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 quick">
                            <h4>Get in Touch</h4>
                            <address>

                                <a href="mailto:info@freefitness.london"><i class="fa fa-envelope-o"></i>info@freefitness.london</a>
                            </address>
                        </div>
                    </div>
                </div>
                <div class="copy_right">
                  <div class="container">
                       <h6>Copyright © Free Fitness London 2018. All rights reserved. </h6>
                        <h6>Created by: <a href="http://www.creativerogue.co.uk/" target="_blank">Creative Rogue</a></h6>
                  </div>
                </div>
            </footer>
            <!--================End Footer Area =================-->

<!-- The Modal -->
<div class="modal" id="newsletter">
  <div class="modal-dialog">
    <div class="modal-content">
<form action="https://london.us17.list-manage.com/subscribe/post?u=d214b685e9e9d3e35806f7706&amp;id=73e20b1431" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
#mc-embedded-subscribe-form input[type=checkbox]{display: inline; width: auto;margin-right: 10px;}
#mergeRow-gdpr {margin-top: 20px;}
#mergeRow-gdpr fieldset label {font-weight: normal;}
#mc-embedded-subscribe-form .mc_fieldset{border:none;min-height: 0px;padding-bottom:0px;}
</style>
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Newsletter subscription</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">


<div id="mc_embed_signup">

    <div id="mc_embed_signup_scroll">
<h2>Notify me about Free Fitness London news and activities</h2>
<div class="mc-field-group">
<label for="mce-EMAIL">Email Address </label>
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
<label for="mce-FNAME">First Name </label>
<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
<label for="mce-LNAME">Last Name </label>
<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
<label for="mce-MMERGE3">What is your Favourite Fitness Activity? </label>
<input type="text" value="" name="MMERGE3" class="required" id="mce-MMERGE3">
</div>
<div id="mergeRow-gdpr" class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group">
    <div class="content__gdpr">
        <label>Marketing Permissions</label>
        <p>Please select all the ways you would like to hear from Free Fitness London:</p>
        <fieldset class="mc_fieldset gdprRequired mc-field-group" name="interestgroup_field">
<label class="checkbox subfield" for="gdpr_19059"><input type="checkbox" id="gdpr_19059" name="gdpr[19059]" value="Y" class="av-checkbox "><span>Email</span> </label>
        </fieldset>
        <p>You can unsubscribe at any time by clicking the link in the footer of our emails.</p>
    </div>
    <div class="content__gdprLegal">
        <p>We use Mailchimp as our marketing platform. By clicking below to subscribe, you acknowledge that your information will be transferred to Mailchimp for processing. <a href="https://mailchimp.com/legal/" target="_blank">Learn more about Mailchimp's privacy practices here.</a></p>
    </div>
</div>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d214b685e9e9d3e35806f7706_73e20b1431" tabindex="-1" value=""></div>
    <div class="clear"></div>
    </div>

</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->


      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary col m-0">

        <button type="button" class="btn btn-primary col" data-dismiss="modal">Close</button>
      </div>
      </form>

    </div>
  </div>
</div>




<!-- Extra Plugin -->

<script src="{{ asset('js/app.js') }}" ></script>
<script src="{{ asset('vendor/lightbox/simpleLightbox.min.js') }}" ></script>
<script src="{{ asset('vendor/magnify-popup/jquery.magnific-popup.min.js') }}" ></script>
<script src="{{ asset('vendor/isotope/imagesloaded.pkgd.min.js') }}" ></script>
<script src="{{ asset('vendor/isotope/isotope.pkgd.min.js') }}" ></script>
<script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}" ></script>

<script src="{{ asset('vendor/counterup/jquery.waypoints.min.js') }}" ></script>
<script src="{{ asset('vendor/counterup/jquery.counterup.min.js') }}" ></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>

<script src="{{ asset('js/gmaps.min.js') }}" ></script>

<script src="{{ asset('js/popper.min.js') }}" ></script>
<script src="{{ asset('js/theme.js') }}" ></script>
<script src="{{ asset('js/contact.js') }}" ></script>
<script src="{{ asset('js/jquery.validate.min.js') }}" ></script>




<!-- Theme js / Coustom js -->

 <script>

  $('#calendar').fullCalendar({
  defaultView: 'agendaWeek'
});


  </script>
  <script type="text/javascript" src="https://s.skimresources.com/js/126632X1588171.skimlinks.js"></script>
</body>
</html>
