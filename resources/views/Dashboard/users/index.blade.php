@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-6">
                       <h1> <a href="{{ route('dashboard') }}">FFL Dashboard |</a/>Current users</h1>
                    </div>
                        <div class="col-md-6">
                                <h1><a class="pull-right" href="{{ route('users.create') }}">Add a new user</a></h1>
                        </div>
                    </div>

                    </div>

                    <div class="card-body">



                                @if (\Session::has('success'))
                                  <div class="alert alert-success">
                                    <p>{{ \Session::get('success') }}</p>
                                  </div><br />
                                 @endif

                                <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Name </th>
                                    <th>Email</th>

                                    <th colspan="2">Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                  @foreach($users as $user)

                                  <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{$user->email}}</td>




                                    <td>
                                      <form action="{{action('UserController@destroy', $user['id'])}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger"

                                        @if ( Auth::user()->id == $user->id)
                                            disabled
                                        @endif

                                        type="submit">Delete</button>
                                      </form>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                              </div>

                            </div>


                        </div>
                    </div>
               </div>
           </div>
                              @endsection
