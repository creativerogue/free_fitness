@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-8">
                      <h1>  <a href="{{ route('dashboard') }}">FFL Dashboard | </a/> Current activities</h1>

                    </div>
                        <div class="col-md-4">
                                <h1><a class="pull-right" href="{{ route('category.create') }}">Create an actitivity</a></h1>
                        </div>
                    </div>

                    </div>


                <div class="card-body">


                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif




                  <div class="row">
                  @foreach($categories as $category)

                  <div class="col-md-3 ">
                        <div class="row">
                 <div class="col-md-12">
                     <h3 id="vAlign" style="background:{{$category->colour}}; " class="align-middle">{{ $category->category }}</h3>
                  </div>
                </div>

                <div class="row mb-30">
                  <div class="col-md-6">
                  <a href="{{action('CategoryController@edit', $category['id'])}}" class="btn btn-warning">Edit</a>
                  </div>

                  <div class="col-md-6">
                  <form action="{{action('CategoryController@destroy', $category['id'])}}" method="post">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                  </form>
                  </div>
                </div>


                  </div>

                  @endforeach
                  </div>


             </div>
         </div>
    </div>
</div>






@endsection
