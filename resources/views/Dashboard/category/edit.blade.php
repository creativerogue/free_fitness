@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('category.index') }}">Activity</a/> | Edit {{$category->category}}</h1>
                    </div>

                    </div>

                    </div>


                <div class="card-body">


                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif
                   <form method="post" action="{{action('CategoryController@update', $id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

                   <div class="row">
                     <div class="col-md-3 col-md-offset-3">
                            <div class="form-group">
                                <lable>Activity</lable>
                                <input type="text" class="form-control" name="category" required value="{{$category->category}}">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <lable>Colour</lable>
                            <select name="colour" required class="form-control">

                        <option value="AliceBlue" @if($category->colour=="AliceBlue") selected @endif>AliceBlue</option>
                        <option value="AntiqueWhite" @if($category->colour=="AntiqueWhite") selected @endif>AntiqueWhite</option>
                        <option value="Aqua" @if($category->colour=="Aqua") selected @endif>Aqua</option>
                        <option value="Aquamarine" @if($category->colour=="Aquamarine") selected @endif>Aquamarine</option>
                        <option value="Azure" @if($category->colour=="Azure") selected @endif>Azure</option>
                        <option value="Beige" @if($category->colour=="Beige") selected @endif>Beige</option>
                        <option value="Bisque" @if($category->colour=="Bisque") selected @endif>Bisque</option>
                        <option value="Black" @if($category->colour=="Black") selected @endif>Black</option>
                        <option value="BlanchedAlmond" @if($category->colour=="BlanchedAlmond") selected @endif>BlanchedAlmond</option>
                        <option value="Blue" @if($category->colour=="Blue") selected @endif>Blue</option>
                        <option value="BlueViolet" @if($category->colour=="BlueViolet") selected @endif>BlueViolet</option>
                        <option value="Brown" @if($category->colour=="Brown") selected @endif>Brown</option>
                        <option value="BurlyWood" @if($category->colour=="BurlyWood") selected @endif>BurlyWood</option>
                        <option value="CadetBlue" @if($category->colour=="CadetBlue") selected @endif>CadetBlue</option>
                        <option value="Chartreuse" @if($category->colour=="Chartreuse") selected @endif>Chartreuse</option>
                        <option value="Chocolate" @if($category->colour=="Chocolate") selected @endif>Chocolate</option>
                        <option value="Coral" @if($category->colour=="Coral") selected @endif>Coral</option>
                        <option value="CornflowerBlue" @if($category->colour=="CornflowerBlue") selected @endif>CornflowerBlue</option>
                        <option value="Cornsilk" @if($category->colour=="Cornsilk") selected @endif>Cornsilk</option>
                        <option value="Crimson" @if($category->colour=="Crimson") selected @endif>Crimson</option>
                        <option value="Cyan" @if($category->colour=="Cyan") selected @endif>Cyan</option>
                        <option value="DarkBlue" @if($category->colour=="DarkBlue") selected @endif>DarkBlue</option>
                        <option value="DarkCyan" @if($category->colour=="DarkCyan") selected @endif>DarkCyan</option>
                        <option value="DarkGoldenRod" @if($category->colour=="DarkGoldenRod") selected @endif>DarkGoldenRod</option>
                        <option value="DarkGrey" @if($category->colour=="DarkGrey") selected @endif>DarkGrey</option>
                        <option value="DarkBlue" @if($category->colour=="DarkBlue") selected @endif>DarkBlue</option>
                        <option value="DarkGreen" @if($category->colour=="DarkGreen") selected @endif>DarkGreen</option>
                        <option value="DarkKhaki" @if($category->colour=="DarkKhaki") selected @endif>DarkKhaki</option>
                        <option value="DarkMagenta" @if($category->colour=="DarkMagenta") selected @endif>DarkMagenta</option>
                        <option value="DarkOliveGreen" @if($category->colour=="DarkOliveGreen") selected @endif>DarkOliveGreen</option>
                        <option value="Darkorange" @if($category->colour=="Darkorange") selected @endif>Darkorange</option>
                        <option value="DarkOliveGreen" @if($category->colour=="DarkOliveGreen") selected @endif>DarkOliveGreen</option>
                        <option value="DarkOrchid" @if($category->colour=="DarkOrchid") selected @endif>DarkOrchid</option>
                        <option value="DarkOliveGreen" @if($category->colour=="DarkOliveGreen") selected @endif>DarkOliveGreen</option>
                        <option value="DarkRed" @if($category->colour=="DarkRed") selected @endif>DarkRed</option>
                        <option value="DarkSalmon" @if($category->colour=="DarkSalmon") selected @endif>DarkSalmon</option>
                        <option value="DarkSeaGreen" @if($category->colour=="DarkSeaGreen") selected @endif>DarkSeaGreen</option>
                        <option value="DarkSlateBlue" @if($category->colour=="DarkSlateBlue") selected @endif>DarkSlateBlue</option>
                        <option value="DarkTurquoise" @if($category->colour=="DarkTurquoise") selected @endif>DarkTurquoise</option>
                        <option value="DarkViolet" @if($category->colour=="DarkViolet") selected @endif>DarkViolet</option>
                        <option value="DeepPink" @if($category->colour=="DeepPink") selected @endif>DeepPink</option>
                        <option value="DeepSkyBlue" @if($category->colour=="DeepSkyBlue") selected @endif>DeepSkyBlue</option>
                        <option value="DimGrey" @if($category->colour=="DimGrey") selected @endif>DimGrey</option>
                        <option value="DodgerBlue" @if($category->colour=="DodgerBlue") selected @endif>DodgerBlue</option>
                        <option value="FireBrick" @if($category->colour=="FireBrick") selected @endif>FireBrick</option>
                        <option value="FloralWhite" @if($category->colour=="FloralWhite") selected @endif>FloralWhite</option>
                        <option value="ForestGreen" @if($category->colour=="ForestGreen") selected @endif>ForestGreen</option>
                        <option value="Fuchsia" @if($category->colour=="Fuchsia") selected @endif>Fuchsia</option>
                        <option value="Gainsboro" @if($category->colour=="Gainsboro") selected @endif>Gainsboro</option>
                        <option value="GhostWhite" @if($category->colour=="GhostWhite") selected @endif>GhostWhite</option>
                        <option value="Gold" @if($category->colour=="Gold") selected @endif>Gold</option>
                        <option value="GoldenRod" @if($category->colour=="GoldenRod") selected @endif>GoldenRod</option>
                        <option value="Grey" @if($category->colour=="Grey") selected @endif>Grey</option>
                        <option value="Green" @if($category->colour=="Green") selected @endif>Green</option>
                        <option value="GreenYellow" @if($category->colour=="GreenYellow") selected @endif>GreenYellow</option>
                        <option value="HoneyDew" @if($category->colour=="HoneyDew") selected @endif>HoneyDew</option>
                        <option value="HotPink" @if($category->colour=="HotPink") selected @endif>HotPink</option>
                        <option value="IndianRed" @if($category->colour=="IndianRed") selected @endif>IndianRed</option>
                        <option value="Indigo" @if($category->colour=="Indigo") selected @endif>Indigo</option>
                        <option value="Ivory" @if($category->colour=="Ivory") selected @endif>Ivory</option>
                        <option value="Khaki" @if($category->colour=="Khaki") selected @endif>Khaki</option>
                        <option value="Lavender" @if($category->colour=="Lavender") selected @endif>Lavender</option>
                        <option value="LavenderBlush" @if($category->colour=="LavenderBlush") selected @endif>LavenderBlush</option>
                        <option value="LawnGreen" @if($category->colour=="LawnGreen") selected @endif>LawnGreen</option>
                        <option value="LemonChiffon" @if($category->colour=="LemonChiffon") selected @endif>LemonChiffon</option>
                        <option value="LightBlue" @if($category->colour=="LightBlue") selected @endif>LightBlue</option>
                        <option value="LightCoral" @if($category->colour=="LightCoral") selected @endif>LightCoral</option>
                        <option value="LightCyan" @if($category->colour=="LightCyan") selected @endif>LightCyan</option>
                        <option value="LightGoldenRodYellow" @if($category->colour=="LightGoldenRodYellow") selected @endif>LightGoldenRodYellow</option>
                        <option value="LightGrey" @if($category->colour=="LightGrey") selected @endif>LightGrey</option>
                        <option value="LightGreen" @if($category->colour=="LightGreen") selected @endif>LightGreen</option>
                        <option value="LightPink" @if($category->colour=="LightPink") selected @endif>LightPink</option>
                        <option value="LightSalmon" @if($category->colour=="LightSalmon") selected @endif>LightSalmon</option>
                        <option value="LightSeaGreen" @if($category->colour=="LightSeaGreen") selected @endif>LightSeaGreen</option>
                        <option value="LightSkyBlue" @if($category->colour=="LightSkyBlue") selected @endif>LightSkyBlue</option>
                        <option value="LightSlateGrey" @if($category->colour=="LightSlateGrey") selected @endif>LightSlateGrey</option>
                        <option value="LightSteelBlue" @if($category->colour=="LightSteelBlue") selected @endif>LightSteelBlue</option>
                        <option value="LightYellow" @if($category->colour=="LightYellow") selected @endif>LightYellow</option>
                        <option value="Lime" @if($category->colour=="Lime") selected @endif>Lime</option>
                        <option value="LimeGreen" @if($category->colour=="LimeGreen") selected @endif>LimeGreen</option>
                        <option value="Linen" @if($category->colour=="Linen") selected @endif>Linen</option>
                        <option value="Magenta" @if($category->colour=="Magenta") selected @endif>Magenta</option>
                        <option value="Maroon" @if($category->colour=="Maroon") selected @endif>Maroon</option>
                        <option value="MediumAquaMarine" @if($category->colour=="MediumAquaMarine") selected @endif>MediumAquaMarine</option>
                        <option value="MediumBlue" @if($category->colour=="MediumBlue") selected @endif>MediumBlue</option>
                        <option value="MediumOrchid" @if($category->colour=="MediumOrchid") selected @endif>MediumOrchid</option>
                        <option value="MediumPurple" @if($category->colour=="MediumPurple") selected @endif>MediumPurple</option>
                        <option value="MediumSeaGreen" @if($category->colour=="MediumSeaGreen") selected @endif>MediumSeaGreen</option>
                        <option value="MediumSlateBlue" @if($category->colour=="MediumSlateBlue") selected @endif>MediumSlateBlue</option>
                        <option value="MediumSpringGreen" @if($category->colour=="MediumSpringGreen") selected @endif>MediumSpringGreen</option>
                        <option value="MediumTurquoise" @if($category->colour=="MediumTurquoise") selected @endif>MediumTurquoise</option>
                        <option value="MediumVioletRed" @if($category->colour=="MediumVioletRed") selected @endif>MediumVioletRed</option>
                        <option value="MidnightBlue" @if($category->colour=="MidnightBlue") selected @endif>MidnightBlue</option>
                        <option value="MintCream" @if($category->colour=="MintCream") selected @endif>MintCream</option>
                        <option value="MistyRose" @if($category->colour=="MistyRose") selected @endif>MistyRose</option>
                        <option value="Moccasin" @if($category->colour=="Moccasin") selected @endif>Moccasin</option>
                        <option value="NavajoWhite" @if($category->colour=="NavajoWhite") selected @endif>NavajoWhite</option>
                        <option value="Navy" @if($category->colour=="Navy") selected @endif>Navy</option>
                        <option value="OldLace" @if($category->colour=="OldLace") selected @endif>OldLace</option>
                        <option value="Olive" @if($category->colour=="Olive") selected @endif>Olive</option>
                        <option value="OliveDrab" @if($category->colour=="OliveDrab") selected @endif>OliveDrab</option>
                        <option value="Orange" @if($category->colour=="Orange") selected @endif>Orange</option>
                        <option value="OrangeRed" @if($category->colour=="OrangeRed") selected @endif>OrangeRed</option>
                        <option value="Orchid" @if($category->colour=="Orchid") selected @endif>Orchid</option>
                        <option value="PaleGoldenRod" @if($category->colour=="PaleGoldenRod") selected @endif>PaleGoldenRod</option>
                        <option value="PaleGreen" @if($category->colour=="PaleGreen") selected @endif>PaleGreen</option>
                        <option value="PaleTurquoise" @if($category->colour=="PaleTurquoise") selected @endif>PaleTurquoise</option>
                        <option value="PaleVioletRed" @if($category->colour=="PaleVioletRed") selected @endif>PaleVioletRed</option>
                        <option value="PapayaWhip" @if($category->colour=="PapayaWhip") selected @endif>PapayaWhip</option>
                        <option value="PeachPuff" @if($category->colour=="PeachPuff") selected @endif>PeachPuff</option>
                        <option value="Peru" @if($category->colour=="Peru") selected @endif>Peru</option>
                        <option value="Pink" @if($category->colour=="Pink") selected @endif>Pink</option>
                        <option value="Plum" @if($category->colour=="Plum") selected @endif>Plum</option>
                        <option value="PowderBlue" @if($category->colour=="PowderBlue") selected @endif>PowderBlue</option>
                        <option value="Purple" @if($category->colour=="Purple") selected @endif>Purple</option>
                        <option value="Red" @if($category->colour=="Red") selected @endif>Red</option>
                        <option value="RosyBrown" @if($category->colour=="RosyBrown") selected @endif>RosyBrown</option>
                        <option value="RoyalBlue" @if($category->colour=="RoyalBlue") selected @endif>RoyalBlue</option>
                        <option value="SaddleBrown" @if($category->colour=="SaddleBrown") selected @endif>SaddleBrown</option>
                        <option value="Salmon" @if($category->colour=="Salmon") selected @endif>Salmon</option>
                        <option value="SandyBrown" @if($category->colour=="SandyBrown") selected @endif>SandyBrown</option>
                        <option value="SeaGreen" @if($category->colour=="SeaGreen") selected @endif>SeaGreen</option>
                        <option value="SeaShell" @if($category->colour=="SeaShell") selected @endif>SeaShell</option>
                        <option value="Sienna" @if($category->colour=="Sienna") selected @endif>Sienna</option>
                        <option value="Silver" @if($category->colour=="Silver") selected @endif>Silver</option>
                        <option value="SkyBlue" @if($category->colour=="SkyBlue") selected @endif>SkyBlue</option>
                        <option value="SlateBlue" @if($category->colour=="SlateBlue") selected @endif>SlateBlue</option>
                        <option value="SlateGrey" @if($category->colour=="SlateGrey") selected @endif>SlateGrey</option>
                        <option value="Snow" @if($category->colour=="Snow") selected @endif>Snow</option>
                        <option value="SpringGreen" @if($category->colour=="SpringGreen") selected @endif>SpringGreen</option>
                        <option value="SteelBlue" @if($category->colour=="SteelBlue") selected @endif>SteelBlue</option>
                        <option value="Tan" @if($category->colour=="Tan") selected @endif>Tan</option>
                        <option value="Teal" @if($category->colour=="Teal") selected @endif>Teal</option>
                        <option value="Thistle" @if($category->colour=="Thistle") selected @endif>Thistle</option>
                        <option value="Tomato" @if($category->colour=="Tomato") selected @endif>Tomato</option>
                        <option value="Turquoise" @if($category->colour=="Turquoise") selected @endif>Turquoise</option>
                        <option value="Violet" @if($category->colour=="Violet") selected @endif>Violet</option>
                        <option value="Wheat" @if($category->colour=="Wheat") selected @endif>Wheat</option>
                        <option value="White" @if($category->colour=="White") selected @endif>White</option>
                        <option value="WhiteSmoke" @if($category->colour=="WhiteSmoke") selected @endif>WhiteSmoke</option>
                        <option value="Yellow" @if($category->colour=="Yellow") selected @endif>Yellow</option>
                        <option value="YellowGreen" @if($category->colour=="YellowGreen") selected @endif>YellowGreen</option>
                       </select>


                        </div>

                      <div class="form-group col-md-3">
                            <lable>&nbsp;</lable>
                        <button type="submit" class="btn btn-success form-control">Submit</button>
                      </div>
                    </div>
                  </form>

             </div>
         </div>
    </div>
</div>

@endsection
