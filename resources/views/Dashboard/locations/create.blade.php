@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('locations.index') }}">Locations</a/> | Add a location</h1>
                    </div>

                    </div>

                    </div>




                <div class="card-body">




                    <form method="post" action="{{action('LocationController@store')}}">
        @csrf

       <div class="row">


                    <div class="form-group col-md-6 ">

                        <input type="text" class="form-control" placeholder="Enter new location" name="location" required value="">
                    </div>

              <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>

             </div>




       </div>

      </form>
      </div>


    </div>
</div>
</div>
</div>


@endsection

