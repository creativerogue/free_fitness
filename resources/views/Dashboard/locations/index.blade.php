@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                        <div class="card-header">
                                <div class="row justify-content-center">
                                <div class="col-md-8">
                          <h1>  <a href="{{ route('dashboard') }}">FFL Dashboard | </a/> Current locations</h1>

                        </div>
                            <div class="col-md-4">
                                    <h1><a class="pull-right" href="{{ route('locations.create') }}">Add a new location</a></h1>
                            </div>
                        </div>

                        </div>


                    <div class="card-body">



                                @if (\Session::has('success'))
                                  <div class="alert alert-success">
                                    <p>{{ \Session::get('success') }}</p>
                                  </div><br />
                                 @endif

                                <table class="table table-striped">
                                <thead>
                                  <tr>

                                    <th>Location</th>
                                     <th colspan="2">Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                  @foreach($locations as $location)

                                  <tr>

                                    <td>{{ $location->location }}</td>



                                    <td><a href="{{action('LocationController@edit', $location->id)}}" class="btn btn-warning">Edit</a></td>

                                    <td>
                                      <form action="{{action('LocationController@destroy', $location->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger"


                                        type="submit">Delete</button>
                                      </form>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                              </div>

                            </div>


                        </div>
                    </div>
               </div>
           </div>
                              @endsection
