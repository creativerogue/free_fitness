@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('locations.index') }}">Locations</a/> | Edit {{$locations->location}}</h1>
                    </div>

                    </div>

                    </div>



                <div class="card-body">

                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                          <p>{{ \Session::get('success') }}</p>
                        </div><br />
                       @endif




                    <form method="post" action="{{action('LocationController@update', $id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
       <div class="row">


                    <div class="form-group col-md-6 ">

                        <input type="text" class="form-control" placeholder="Enter new location" name="location" required value="{{$locations->location}}">
                    </div>

              <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>

             </div>




       </div>

      </form>
      </div>


    </div>
</div>
</div>
</div>


@endsection

