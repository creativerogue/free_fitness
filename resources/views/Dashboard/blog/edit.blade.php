@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row justify-content-center">
     <div class="col-md-12">
      <div class="card">

        <div class="card-header">
          <div class="row justify-content-center">
           <div class="col-md-12">
             <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('articles.index') }}">Articles</a/></h1>
              <h3>Edit | {{$article->title}}</h3>
            </div>
          </div>
         </div>

        <div class="card-body">

             @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif
     
      @if(count($errors) > 0)
    <div id="error-messages" class="alert alert-danger " role="alert">
     
        
        @foreach($errors->all() as $error)

            {{ $error }}<br />

        @endforeach
    </div>
@endif


           <div class="row justify-content-center">

           <div class="table-responsive col-md-12">
        <table class="table">
            <tbody>

                <tr><th>Title</th><td>{{$article->title}}</td><th></th><td></td></tr>
                <tr><th> Author</th><td>{{ $article->author }}</td><th>Category</th><td>{{$article->category}}</td></tr>
                <tr><th>Category</th><td>{{$article->category}}</td><th>Date Published</th><td>{{ $article->created_at->format('d/m/Y') }}</td></tr>
                <tr><th>Featured Image </th><td><img src="{{ $article->image }}" class="img-thumbnail" style= "width:auto; height:100px;"/></td><th></th><td></td></tr>

            </tbody>
        </table>
    </div>
        <div class="table-responsive col-md-12">
                <table class="table">
                    <tbody>
        <tr><td>@php Print_r($article->body);@endphp</td></tr>

    </tbody>
</table>
    </div>
                   </div>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
  Edit this article
</button>


<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
     <div class="modal-content">
       <div class="modal-header">
  
        <h5 class="modal-title" id="ModalLabel">Edit {{$article->title}}</h5>
         <button type="button" class="close " data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
         </button>
      
      </div>


      <form method="post" action="{{action('ArticleController@update', $id)}}">
        @csrf
       <input name="_method" type="hidden" value="PATCH">
     
      <div class="modal-body">

        <div class="container">
          
        <div class="row">

           <div class="form-group col-md-12 ">
           
            <label>Title:</label>
            <input type="text" class="form-control" name="title" value="{{$article->title}}"> 
           
           </div>

          <div class="form-group col-md-12 ">
            
            <label>Featured Image</label>
             <div class="input-group">
              <span class="input-group-btn">
               <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                <i class="fas fa-camera"></i> Choose
               </a>
              </span>
               <input id="thumbnail" class="form-control" type="text" name="image" value="{{$article->image}}">
             </div>
             <img id="holder" style="margin-top:15px;max-height:100px;">
         
           </div>

          </div>
      
        <div class="row">
         <div class="form-group col-md-12">
         
           <label>Article:</label>
            <textarea class="form-control my-editor" name="body" {!!$article->body!!}</textarea>
         
         </div>
        </div>
 
        <div class="row form-group">
         <div class=" col-md-6">
                    
          <label>Category</label>
           <select name="category" class="form-control">
             <option value="{{$article->category}}" >{{$article->category}}</option>
               @foreach($categories as $category)
                <option value="{{$category->category}}" >{{$category->category}}</option>
               @endforeach
           </select>
          
         </div>

         <div class=" col-md-6">

            @if (Auth::user()->name == $article->author)
                  <label>You are the author</label>
            <input type="text" class="form-control" readonly name="author" value="{{$article->author}}"> 
            @else
           
           <label>Change Author</label>
            <select name="author" class="form-control">
               @foreach($users as $user)
               
                <option @if ($user->name == Auth::user()->name) echo "selected" @endif
                 value="{{$user->name}}" >{{$user->name}}</option>
               @endforeach
          
            </select>
          @endif

         </div>
        </div>
          </div>
        </div>

        <div class="modal-footer">
        <div class="container">
          <div class="row form-group ">
        
           <div class="col-md-6">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            
            <div class="col-md-6">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
         
         </div>

           </div>
          </div>

          </form>
           
   
</div>
</div>
</div>


@endsection

