@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('articles.index') }}">Article</a/> | Create a new article</h1>
                    </div>

                    </div>

                    </div>



                <div class="card-body">

             @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif
     
      @if(count($errors) > 0)
    <div id="error-messages" class="alert alert-danger" role="alert">
               
        @foreach($errors->all() as $error)

            {{ $error }}<br />

        @endforeach
    </div>
@endif



                    <form method="post" action="{{action('ArticleController@store')}}">
        @csrf

       <div class="row">
         <div class="col-md-12">

                <div class="row">

                 <div class="form-group col-md-12 ">
              <label>Title:</label>
              <input type="text" class="form-control" name="title" >
            </div>

            

               <div class="form-group col-md-12">
                    <label>Featured Image</label>
                <div class="input-group">
                    <span class="input-group-btn">
                      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                      </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image">
                  </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">
              </div>

            

             </div>
      

  <div class="row">
                <div class="col-md-12">
                  <div class="form-group  ">
                      <label>Article:</label>
                      <textarea rows=9 class="form-control my-editor" name="body"></textarea>
                    </div>
                  </div>
                  </div>

  <div class="row">
                      <div class="form-group col-md-6">
                    <label>Category</label>
                    <select name="category" class="form-control">
                            @foreach($categories as $category)
                            <option value="{{$category->category}}" >{{$category->category}}</option>

                            @endforeach

                    </select>
                </div>
                
                    <div class="form-group col-md-6">
                     <label>Author - <b>{{Auth::user()->name}}</b></label>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>

       </div>

      </form>
      </div>

</div>
    </div>
</div>
</div>
</div>


@endsection

