@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header"><h1>FFL Dashboard</h1></div>

            <div class="card-body">

             <div class="row justify-content-center">
              <div class="col-md-11 col-md-offset-1" id="dash-buttons">
   
                <a href="dashboard/users" style='text-align:center;' class='find_btn btn-lg m-4 ' > 
                 <div style='text-align:center;'><i class="fas fa-user-edit fa-7x p-3 "></i></div>  
                   Users
                </a>
                            
                <a href="dashboard/articles" style='text-align:center;' class='find_btn btn-lg m-4' > 
                 <div style='text-align:center;'><i class="far fa-comment fa-7x p-3"></i></div>  
                   Articles
                </a>

                <a id="lfm"  style='text-align:center;' class='find_btn btn-lg m-4' > 
                 <div style='text-align:center;'><i class="fas fa-camera-retro fa-7x p-3 "></i></div>  
                   Media Library
                </a>

              

                <a href="dashboard/content" style='text-align:center;' class='find_btn btn-lg m-4' > 
                 <div style='text-align:center;'><i class="fas fa-database fa-7x p-3 "></i></div> 
                    Page content
                </a>

             </div>
            </div>

            <div class="row justify-content-center">
             <div class="col-md-11 col-md-offset-1" id="dash-buttons">

               <a href="dashboard/category" style='text-align:center;' class='find_btn btn-lg m-4' > 
                <div style='text-align:center;'><i class="fas fa-futbol fa-7x p-3"></i></div>  
                  Activities
               </a>

               <a href="dashboard/locations" style='text-align:center;' class='find_btn btn-lg m-4' > 
                <div style='text-align:center;'><i class="fas fa-map-marker-alt  fa-7x p-3"></i></div>  
                Locations
               </a>
  
               <a href="dashboard/clubs" style='text-align:center;' class='find_btn btn-lg m-4' > 
                <div style='text-align:center;'><i class="fas fa-trophy fa-7x p-3"></i></div>  
                 Clubs
               </a>

               <a href="dashboard/events" style='text-align:center;' class='find_btn btn-lg m-4' > 
                <div style='text-align:center;'><i class="fas fa-clock fa-7x p-3 "></i></div>  
                 Events
               </a>

               
           
           
           

                
             </div>
            </div>
                     
          </div>
      </div>
   </div>
</div>






@endsection
