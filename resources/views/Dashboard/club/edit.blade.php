@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('clubs.index') }}">Clubs</a/> | Edit {{$club->name}}</h1>
                    </div>

                    </div>

                    </div>

                <div class="card-body">


                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif

                   <div class="row justify-content-center">

<div class="table-responsive col-md-6">
        <table class="table">
            <tbody>

                <tr><th> Activity </th><td> {{ $club->category }} </td></tr>
                <tr><th> Community </th><td>{{$club->name}}</td></tr>
                <tr><th> Location </th><td>{{$club->location}}</td></tr>

               <tr><th> Image </th><td><img src="{{ $club->logo }}" class="img-thumbnail" style= "width:auto; height:100px;"/></td></tr>

            </tbody>
        </table>
    </div>
        <div class="table-responsive col-md-6">
                <table class="table">
                    <tbody>
        <tr><th> About </th><td>@php Print_r($club->description);@endphp</td></tr>

    </tbody>
</table>
    </div>
                   </div>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
  Edit {{$club->name}}
</button>


<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="ModalLabel">Edit {{$club->name}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">




            <form method="post" action="{{action('ClubController@update', $id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="PATCH">

           <div class="row">

                  <div class="form-group col-md-6">
                    <label>Activity</label>
                  <select name="category" class="form-control">
                      <option value="{{$club->category}}" >{{$club->category}}</option>

                       @foreach($categories as $category)
                              <option value="{{$category->category}}" >{{$category->category}}</option>
                        @endforeach
                     </select>
                    </div>

                <div class="form-group col-md-6">
                  <label>Community:</label>
                  <input type="text" class="form-control" name="name" value="{{$club->name}}">
                </div>

                <div class="form-group col-md-6">
                <label>Location</label>


                <select name="location" class="form-control">
                        <option value="{{$club->location}}" >{{$club->location}}</option>

                        @foreach($locations as $location)
                        <option value="{{$location->location}}" >{{$location->location}}</option>

                        @endforeach
                       </select>


           </div>

                <div class="form-group col-md-6">
                        <label>Logo:</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Choose
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="logo" value="{{$club->logo}}">
                          </div>
                          <img id="holder" style="margin-top:15px;max-height:100px;">
                  </div>

                <div class="form-group col-md-12">
                          <label for="Community">About:</label>
                          <textarea class="form-control my-editor" name="description">{{$club->description}}</textarea>
                        </div>



                        <div class="form-group col-md-12">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </div>
      </div>

      </div>

    </div>
</div>
</div>
</div>


@endsection

