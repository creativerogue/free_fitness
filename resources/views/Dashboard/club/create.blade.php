@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('clubs.index') }}">Club</a/> | Create a new club</h1>
                    </div>

                    </div>

                    </div>



                <div class="card-body">




                    <form method="post" action="{{action('ClubController@store')}}">
        @csrf

       <div class="row">
         <div class="col-md-6 ">

                <div class="row">

                <div class="form-group col-md-12">
                    <label>Activity</label>
                    <select name="category" class="form-control">
                            @foreach($categories as $category)
                            <option value="{{$category->category}}" >{{$category->category}}</option>

                            @endforeach

                    </select>
                </div>

            <div class="form-group col-md-12 ">
              <label>Community:</label>
              <input type="text" class="form-control" name="name" value="" required>
            </div>


                   <div class="form-group col-md-12 ">
                       <label>Location</label>

                       <select name="location" class="form-control">
                            @foreach($locations as $location)
                            <option value="{{$location->location}}" >{{$location->location}}</option>

                            @endforeach

                    </select>

                   </div>



               <div class="form-group col-md-12 ">
                    <label>Logo</label>
                <div class="input-group">
                    <span class="input-group-btn">
                      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                      </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="logo" required >
                  </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">
              </div>

              <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>

             </div>
         </div>


                <div class="col-md-6">
                  <div class="form-group  ">
                      <label>About:</label>
                      <textarea rows=9 class="form-control my-editor" name="description"></textarea>
                    </div>
                  </div>

       </div>

      </form>
      </div>


    </div>
</div>
</div>
</div>


@endsection

