@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                        <div class="card-header">
                                <div class="row justify-content-center">
                                <div class="col-md-8">
                          <h1>  <a href="{{ route('dashboard') }}">FFL Dashboard | </a/> Articles</h1>

                        </div>
                            <div class="col-md-4">
                                    <h1><a class="pull-right" href="{{ route('clubs.create') }}">Add a new club</a></h1>
                            </div>
                        </div>

                        </div>



                    <div class="card-body">



                                @if (\Session::has('success'))
                                  <div class="alert alert-success">
                                    <p>{{ \Session::get('success') }}</p>
                                  </div><br />
                                 @endif

                                <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Activity </th>
                                    <th>Community</th>

                                    <th colspan="2">Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                  @foreach($clubs as $club)

                                  <tr>
                                    <td>{{ $club->id }}</td>
                                    <td>{{ $club->category }}</td>
                                    <td>{{$club->name}}</td>


                                    <td><a href="{{action('ClubController@edit', $club['id'])}}" class="btn btn-warning">Edit</a></td>
                                    <td>
                                      <form action="{{action('ClubController@destroy', $club['id'])}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                      </form>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                              </div>

                            </div>


                        </div>
                    </div>
               </div>
           </div>
                              @endsection
