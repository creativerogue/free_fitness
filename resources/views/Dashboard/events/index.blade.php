@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-8">
                      <h1>  <a href="{{ route('dashboard') }}">FFL Dashboard | </a/> Current events</h1>

                    </div>
                        <div class="col-md-4">
                                <h1><a class="pull-right"href="{{ route('events.create') }}">Add an event</a></h1>
                        </div>
                    </div>

                    </div>


                <div class="card-body">

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif


              
                    <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Table View</a>
                              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Calander View</a>

                            </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">







                          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table class="table table-striped">
                                        <thead>
                                          <tr>
                                           <th>Club</th>
                                            <th>Event name</th>
                                             <th>Event day</th>
                                             <th>Start Time</th>
                                             <th>End Time</th>
                                             <th>All day?</th>
                                             <th>Recurring?</th>
                                             <th>Frequency?</th>
                                          </tr>
                                        </thead>
                                        <tbody>


                                @foreach($events as $event)
                           <tr>
                             <td> {{$event->club->name}}</td>
                             <td> {{$event->event_name}}</td>
                             <td> {{$event->event_day}}</td>
                             <td> {{$event->startTime}}</td>
                             <td> {{$event->endTime}}</td>
                              <td>
                                @if ($event->all_day=='1')
                                   Yes
                                    @else
                                    No
                                 @endif
                              </td>
                              <td>
                                 @if ($event->reccuring =='1')
                                     Yes
                                      @else
                                       No
                                      @endif
                                 </td>
                                 <td>@php switch ($event->frequency):
                                        case 0:
                                        echo"None";
                                        break;
                                        case 1:
                                        echo"Weekly";
                                        break;
                                        case 2:
                                        echo"Fortnightly";
                                        break;
                                        case 4:
                                        echo"Monthly";
                                        break;
                                        endswitch;
                                        @endphp

                                </td>
                                 <td><a href="{{action('EventController@edit', $event['id'])}}" class="btn btn-warning">Edit</a></td>
                                 <td>
                                   <form action="{{action('EventController@destroy', $event['id'])}}" method="post">
                                     @csrf
                                     <input name="_method" type="hidden" value="DELETE">
                                     <button class="btn btn-danger" type="submit">Delete</button>
                                   </form>
                                 </td>
                               </tr>
                          @endforeach



                                        </tbody>
                                      </table>





        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                {!! $calendar_details->calendar() !!}
                {!! $calendar_details->script() !!}


        </div>

      </div>





                </div>
            </div>
        </div>
    </div>

@endsection


