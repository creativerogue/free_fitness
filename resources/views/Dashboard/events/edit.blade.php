@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              

  <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('events.index') }}">Events</a/> | Edit {{$event->event_name}}</h1>
                    </div>
                      </div>
                        </div>


                <div class="card-body">
                        <h2>Edit {{$event->event_name}}</h2>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                    <form method="post" action="{{action('EventController@update', $id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                            <div class="row">
                               <div class="col-md-12">

                                @if (Session::has('Success'))
                                <div class="alert alert-success">{{ Session::get('Success') }}</div>
                                @elseif (Session::has('Warning'))
                                <div class="alert alert-danger">{{ Session::get('Warning') }}</div>
                               </div>
                               @endif
                            </div>
                        </div>

                        <div class="row">
                         <div class="col-md-3">
                            <div class="form-group">
                              <label>Club</label>
                                <select name="club_id" class="form-control">
                                        @foreach($club as $x)
                                        <option value="{{$x->id}}" >{{$x->name}}</option>
                                         @endforeach

                                     @foreach($clubs as $club)
                                       <option value="{{$club->id}}" >{{$club->name}}</option>
                                        @endforeach
                                     </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Event name:</label>
                                        <input type="text" class="form-control"  name="event_name" value="{{$event->event_name}}" >
                                    </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Event location:</label>
                                            <input type="text" class="form-control"  name="event_location" value="{{$event->event_location}}">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                      <div class="form-group">
                                         <label>Week day:</label>
                                            <select name="event_day" class="form-control">
                                              <option selected value="0" >None</option>
                                              <option value="Sunday" @if($event->event_day =="Sunday") selected @endif>Sunday</option>
                                              <option value="Monday" @if($event->event_day =="Monday") selected @endif>Monday</option>
                                              <option value="Tuesday" @if($event->event_day =="Tuesday") selected @endif>Tuesday</option>
                                              <option value="Wednesday" @if($event->event_day =="Wednesday") selected @endif>Wednesday</option>
                                              <option value="Thursday" @if($event->event_day =="Thursday") selected @endif>Thursday</option>
                                              <option value="Friday" @if($event->event_day =="Friday") selected @endif>Friday</option>
                                              <option value="Saturday" @if($event->event_day =="Saturday") selected @endif>Saturday</option>


                                              </select>
                                       </div>
                                   </div>
                                </div>
                                <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Date:</label>
                                        <input type="date" class="form-control"  name="startDate" value="{{$event->startDate}}">
                                    </div>
                                </div>

                                <div class="col-md-1">




                                        <label >All day?</label>
                                        <div class="form-check">

                                            <input type="checkbox" class="form-check-input " @if ($event->all_day == 1) checked @endif id="all_day" name="all_day" value="1">

                                          </div>

                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Start time:</label>
                                        <input type="time" class="form-control"  name="startTime" value="{{$event->startTime}}">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>End time:</label>
                                        <input type="time" class="form-control"  name="endTime" value="{{$event->endTime}}">
                                    </div>
                                </div>


                                <div class="col-md-1">

                              <label>Recurring?</label>
                                <div class="form-check">

                                  <input type="checkbox" class="form-check-input " @if ($event->reccuring == 1) checked @endif id="reccuring" name="reccuring" value="1">
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                  <label>Frequency</label>
                                    <select name="frequency" class="form-control">

                                     <option value="0" @if($event->frequency =="0") selected @endif>None</option>
                                     <option value="1" @if($event->frequency =="1") selected @endif>Weekly</option>
                                     <option value="2" @if($event->frequency =="2") selected @endif>Fortnightly</option>
                                     <option value="4" @if($event->frequency =="4") selected @endif>Monthly</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>

                                <div class="col-md-6 text-center">
                                        <label>&nbsp;</label>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>


@endsection





