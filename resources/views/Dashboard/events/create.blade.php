@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
          <div class="card-header">
                            <div class="row justify-content-center">
                            <div class="col-md-12">
                        <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/><a href="{{ route('events.index') }}">Events</a/> | Add a new event </h1>
                    </div>
                      </div>
                        </div>

                <div class="card-body">
                        

      

                    <form method="post" action="{{action('EventController@store')}}" enctype="multipart/form-data">
                        @csrf
                            @csrf

                             <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Club</label>
                                        <select required name="club_id" class="form-control">
                                                <option value="" >Please select..</option>
                                                @foreach($clubs as $club)
                                                <option value="{{$club->id}}" >{{$club->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Event name:</label>
                                        <input type="text" class="form-control"  name="event_name" required >
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Event location:</label>
                                        <input type="text" class="form-control"  name="event_location" required>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                  <div class="form-group">
                                     <label>Week day:</label>
                                        <select name="event_day" class="form-control">
                                          <option selected value="0" >None</option>
                                          <option value="Sunday" >Sunday</option>
                                          <option value="Monday" >Monday</option>
                                          <option value="Tuesday" >Tuesday</option>
                                          <option value="Wednesday" >Wednesday</option>
                                          <option value="Thursday" >Thursday</option>
                                          <option value="Friday" >Friday</option>
                                          <option value="Saturday" >Saturday</option>

                                          </select>
                                   </div>
                               </div>
                            </div>
                                <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Date:</label>
                                        <input type="date" class="form-control"  name="startDate" required >
                                    </div>
                                </div>

                                <div class="col-md-1">

                                        <label >All day?</label>
                                        <div class="form-check">

                                            <input type="checkbox" class="form-check-input " id="all_day" name="all_day" value="1">

                                          </div>

                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Start time:</label>
                                        <input type="time" class="form-control"  name="startTime" >
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>End time:</label>
                                        <input type="time" class="form-control"  name="endTime" >
                                    </div>
                                </div>


                                <div class="col-md-1">

                              <label>Recurring?</label>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input " id="reccuring" name="reccuring" value="1">

                                      </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Frequency</label>
                                    <select name="frequency" class="form-control">
                                        <option selected value="0" >None</option>
                                            <option value="1" >Weekly</option>
                                            <option value="2" >Fortnightly</option>
                                            <option value="4" >Monthly</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>

                                <div class="col-md-6 text-center">
                                        <label>&nbsp;</label>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>


@endsection


