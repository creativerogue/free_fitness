@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row justify-content-center">
     <div class="col-md-12">
      <div class="card">

        <div class="card-header">
          <div class="row justify-content-center">
           <div class="col-md-12">
             <h1><a href="{{ route('dashboard') }}">FFL Dashboard  | </a/>Edit Page content</h1>
     
            </div>
          </div>
         </div>

        <div class="card-body">

             @if (\Session::has('success'))
                    <div class="alert alert-success">
                      <p>{{ \Session::get('success') }}</p>
                    </div><br />
                   @endif

         <div class="row text-center">

            <div class="col-md-12">
              <h2>Header Images</h2>
            </div>
          </div>
          
          <div class="blog_area">
           <div class="row blog_inner">

          @foreach ($images as $content)
            
            <div class="col-md-6 blog_items">
              <h4 class="text-center mb-3">{{ $content->name }}</h4>
               <a data-toggle="modal" data-target="#editModal{{$content->id}}" class="blog_img" > <img src="{{ $content->content }}" class="img-thumbnail" style= "width:auto; height:100px;"/></a>
            </div>


           <!--Modal begins-->
            
            <div class="modal fade" id="editModal{{$content->id}}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
               
                <div class="modal-header">
                  <h5 class="modal-title" id="ModalLabel">Edit {{$content->name}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
                </div>
             
             <form method="post" action="{{action('ContentController@update', $content->id)}}">
                    @csrf
                   <input name="_method" type="hidden" value="PATCH">
               
               
                <div class="modal-body">

                     <div class="row">

                      <div class="form-group col-md-10">
                        <label>Header Image:</label>
                       
                        <div class="input-group">
                            <span class="input-group-btn">
                              <a id="lfm{{$content->id}}" data-input="thumbnail{{$content->id}}" data-preview="holder{{$content->id}}" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Choose
                              </a>
                            </span>
                            <input id="thumbnail{{$content->id}}" class="form-control" type="text" name="content" value="{{ $content->content }}">
                          </div>
                          <img id="holder{{$content->id}}" style="margin-top:15px;height:auto;width:100%;" >
</div>
                    <div class="form-group col-md-2 mt-5">
           
                          <input class="form-check-input" type="checkbox"   @if ($content->name === 'Home/Default') checked disabled @endif value="1" name="default" id="default">
                           <label class="form-check-label" for="default">Use Default </label>

                       
                          

                      </div>
    
                    </div>
                
            
                 </div>

                 <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 </div>
                   </form>

          </div>
        </div>
      </div>
   <!--Modal ends-->

          @endforeach
   
        </div>

     
     
       </div>
  <div class="row text-center">

            <div class="col-md-12">
              <h2>Text blocks</h2>
            </div>
          </div>
          
          <div class="blog_area">
           <div class="row blog_inner">

          @foreach ($text as $content)
           
            <div class="col-md-6 blog_items">
              <h4 class="text-center mb-3">{{ $content->name }}</h4>
             <form method="post" action="{{action('ContentController@update', $content->id)}}">
                @csrf
               <input name="_method" type="hidden" value="PATCH">
           
               <textarea class="form-control my-editor" name="content"> {!!$content->content!!}</textarea>

                 <button type="submit" class="btn btn-success">Submit</button>
                  
            </form>

        </div>
          @endforeach
   
        </div>

     
     
       </div>


     </div>
    </div>
   </div>
  </div>






@endsection

