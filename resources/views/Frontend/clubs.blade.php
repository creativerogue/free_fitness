@extends('layouts.frontend')


@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area">
        <h2>Clubs</h2>
        <a href="{{route('home')}}">Home <span>Clubs</span></a>
    </div>



        <!--================Gallery Classic Area =================-->
        <section class="grid_gallery_area">
                <div class="container-fluid">
                    <div class="grid_gallery_inner">
                        <ul class="gallery_filter">
                            <li class="active" data-filter="*"><a href="#">Show All</a></li>

                  @foreach ($activities as $club)

                  @php $clubFIL = str_replace(" ","-",$club ); @endphp
                  <li data-filter=".{{ $clubFIL }}"><a href="#">{{ $club}}</a></li>


                  @endforeach


                        </ul>

                              <!-- Event Items -->

                        <div class="row m-0 grid_gallery_item_inner imageGallery1">

                                @foreach ($clubs as $club)

                                @php $clubFIL = str_replace(" ","-",$club->category ); @endphp
                                <div class="g-item activites {{ $clubFIL }}">
                                    <div class="grid_gallery_item">
                                    <div class="event">
                                            <img src="{{ $club->logo }}" alt="{{ $club->name }}">
                                            <div class="resort_g_hover">
                                                <a class="" href="club/{{ $club->id }}/{{ str_slug($club->name)}}"><i class="icon icon-Linked"></i></a>
                                                <div class="media">

                                                        <div class="media-body">
                                                           <h4><a href="club/{{ $club->id }}/{{ str_slug($club->name)}}" class="w_text">{{ $club->name }}</a></h4>
                                                           <h4><a href="club/{{ $club->id }}/{{ str_slug($club->name)}}" class="w_text">{{ $club->location }}</a></h4>

                                                        </div>
                                                    </div>
                                            </div>


                                    </div>
                                    </div>
                                </div>

                         @endforeach


                        </div>
                    </div>
                </div>
            </section>
            <!--================End Gallery Classic Area =================-->




@endsection

