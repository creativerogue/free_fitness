@extends('layouts.frontend')

@section('content')

<div class="banner_area">
        <h2>Contact Us</h2>
        <a href="#">Home <span>Contact us</span></a>

    </div>
  <!--================End Banner Area =================-->

        <!--================Contact us Area =================-->
        <section class="contact_us_area">
                <div class="container">
                    <div class="row contact_us_row">
                        <div class="col-lg-7 getin_touch">
                            <div class="left_tittle">
                                <h2>Get in touch with us</h2>
                            </div>
                            <form class="row from_main" action="php/contact.php" method="post" id="phpcontactform" novalidate="novalidate">
                                <div class="form-group col-lg-6">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Username">
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email address*">
                                </div>
                                <div class="form-group col-12">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject*">
                                </div>
                                <div class="form-group col-12">
                                    <textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group m-0 col-12">
                                    <button class="find_btn btn" id="js-contact-btn" type="submit">Send message</button>
                                    <div id="js-contact-result" data-success-msg="Form submitted successfully." data-error-msg="Messages Successfully"></div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-5 map_area">
                            <div id="mapBox" class="mapBox"
                                data-lat="40.701083"
                                data-lon="-74.1522848"
                                data-zoom="12"
                                data-marker="images/map-marker.png"
                                data-info="PO Box CT16122 Collins Street West, Victoria 8007, Australia."
                                data-mlat="40.701083"
                                data-mlon="-74.1522848">
                            </div>
                        </div>
                    </div>
                    <div class="row meet_area">
                        <div class="col-lg-4 col-md-6 meet">
                            <i class="flaticon-maps-and-flags"></i>
                            <h3>Meet us</h3>
                            <h6>54B, Tailstoi Town 5238 MT, <br>La city, IA 522364</h6>
                        </div>
                        <div class="col-lg-4 col-md-6 meet">
                            <i class="flaticon-smartphone-call"></i>
                            <h3>Call us</h3>
                            <h6>Phone : (1800) 456 789 <br>Fax : 1200 954 755</h6>
                        </div>
                        <div class="col-lg-4 col-md-6 meet">
                            <i class="flaticon-note"></i>
                            <h3>Call us</h3>
                            <h6>info@fitnesscoach.com <br>contact@fitnesscoach.com</h6>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Contact us Area =================-->




@endsection

