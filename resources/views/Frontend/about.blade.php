@extends('layouts.frontend')

@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area">
        <h2>About us</h2>
        <a href="{{route('home')}}">Home </a><span class="text-white"> About us</span>
    </div>

        <!--================Best fitness  Area =================-->
        <section class="best_fitness_area">
            <div class="container">
                <div class="row best_fitness_row">
                    <div class="col-lg-4">
                        <div class="fitness_img">
                            <img src="images/best-fitness.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8 fitness_content">
                        <div class="left_tittle p-0">
                            <h2>Our Goal</h2>
                            <p>Our goal at free fitness is to provide a platform that brings together all the fantastic fitness opportunities in this great city. We have only one rule and that is all the activities we promote are free!</p>
                        </div>
                        <ul class="fitness_list">
                            <li><img src="images/check.png" alt="">Everyone has the right to keep fit and healthy and money shouldn’t be an obstacle!</li>
                            <li><img src="images/check.png" alt="">You shouldn’t have to pay those eye watering gym memberships and get tied down to long contracts.</li>
                           </ul>
                         <p>
                         Instead you can come to Free Fitness London and we will highlight the most exciting free fitness activities in the city! It’s a fantastic way of meeting likeminded people who have a love for sport and keeping active.</p>
                    </div>
                         </div>
            </div>
        </section>
        
        <!--================Fitness Course Area =================-->
        <section class="fitness_course_area">
            <div class="container">
                <div class="fitness_course_row">
                    <h2>Free fitness london</h2>
                    <p>Find free and local fitness and sports clubs in your area or if you have a club you would like to tell us about, get in touch</p>

                </div>
            </div>
        </section>
        <!--================End Fitness Course Area =================-->
 <section class="best_fitness_area">
            <div class="container">
                <div class="row best_fitness_row">

                    <div class="col-md-6 misssion_content">
                        <div class="left_tittle p-0">
                            <h2>About Free Fitness London</h2>
                        </div>

                        <p>We have spent close to a year researching the best and most interesting groups providing free sports related activities, so we have ensured to only offer you the best activities around.<br/>
                         We also carry out regular reviews on activities on our site to ensure the quality is high.<br/>
                         Remember the activities are free but there is nothing cheap about the quality!</p>
                        <p>We want to build a community that allows people to share their ideas and highlights the coolest things that are happening in the world of fitness. We hope that you will come on this enjoyable journey with us.</p>
                        <p>We have some fun things planned for you guys, so please sign up for the newsletter so we can keep you updated!</p>
                    </div>
                    <div class="col-md-6 misssion_img">
                        <img src="images/misssion.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--================End Best fitness  Area =================-->











@endsection

