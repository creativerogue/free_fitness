@extends('layouts.frontend')

@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area">
        <h2>{{ $club->name }}</h2>
        <a href="{{route('home')}}">Home </a><a href="{{route('clubs')}}">Clubs</a><span class="text-white"> {{ $club->name }}</span>
    </div>

 <!--================Events Details Area =================-->
 <section class="event_details_area">
        <div class="container">
            <div class="row event_details_row">
                <div class="col-lg-6">
                    <div class="details_left_sidebar">
                        <img src="{{ $club->logo }}" alt="{{ $club->name }}">
                           <div class="sharethis-inline-share-buttons"></div>
                        <div class="event_details">


                         <p>@php Print_r($club->description);@endphp</p> 
                       
                        </div>

                      
                    </div>
                </div>
                <div class="col-lg-6">
                   <div class="details_right_sidebar">


 


  @if   ( $events->count() > '0')
     @foreach ($events as $event)


       <h4 class="p-0">Event Name</h4>
       <h6>{{$event->event_name}}</h6>

       <h4>location</h4>
       <address>{{$event->event_location}}</address>
       <h4>Day/Time</h4>
       <h6>{{$event->event_day}} - {{ Carbon\Carbon::parse($event->startTime)->format('g:i:a') }} - {{ Carbon\Carbon::parse($event->endTime)->format('g:i:a') }}</h6>

<br/>
<hr>

    @endforeach
 @else 
   <h4>{{ $club->name }} currently do not have any events.</h4>
 @endif
 

                   </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Events Details Area =================-->

@endsection
