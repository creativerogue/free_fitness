@extends('layouts.frontend')

@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area text-white">
        {!!$headertext!!}

    </div>

     <!--================Health Counter Area =================-->
     <section class="health_counter_area">
            <div class="container">
                <div class="row health_counter_row">
                    <div class="col-lg-3 col-md-6 counter_col">
                    <h1 class="counter">{{$c3}}</h1>
                        <h4>Current activities</h4>
                    </div>
                    <div class="col-lg-3 col-md-6 counter_col">
                        <h1 class="counter">{{$c4}}</h1>
                        <h4>London locations</h4>
                    </div>
                    <div class="col-lg-3 col-md-6 counter_col">
                        <h1 class="counter">{{$c2}}</h1>
                        <h4>Event count</h4>
                    </div>
                    <div class="col-lg-3 col-md-6 counter_col">
                        <h1><span class="counter">{{$c1}}</span></h1>
                        <h4>Registered clubs </h4>
                    </div>
                </div>
            </div>
        </section>

        <!--================Calander Area =================-->
        <section class="best_fitness_area">
          <div class="container">
            <div class="row best_fitness_row">
              <div class=" fitness_content col-md-12">
                <div class="form-container">
                
                <form method="get" action="{{ action('SearchController@Search') }}"> 
                  <div class="row mb-4">
                      
                    
                     
                    <div class="col-md-12 mb-4">
                      <h2 class="text-center">Search events</h2>
                    </div>
                                   
                    <div class="form-group col-md-2">
                     <select class="form-control transparant btn find_btn" id="activity" name="activity" title="activity">
                      <option value="0">Activity</option>
                    @foreach($activities as $activity)
                      <option value="{{ $activity->category }}">{{ $activity->category }}</option>
                    @endforeach
                     </select>
                    </div>
                    
                    <div class="form-group col-md-3">
                     <select class="form-control transparant btn find_btn" id="location" name="location" title="location">
                        <option value="0">location</option>
                      @foreach($locations as $location)
                        <option value="{{ $location->location }}">{{ $location->location }}</option>
                      @endforeach
                     </select>
                    </div>

                    <div class="form-group col-md-3">
                      <select class="form-control transparant btn find_btn" id="club" name="club" title="club">
                       <option value="0">Organiser</option>
                      @foreach($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                      @endforeach
                       </select>
                     </div>
                     
                    <div class="form-group col-md-2">
                     <button type="submit" id="find-contact" name="find-contact" class="find_btn btn mb-2"><i class="fa fa-search   p-3"></i></button>
                    </div>
                     <div class="form-group col-md-2">
                     <a href="/" class="find_btn btn">Reset</a>
                     </div>

                </div>    </form>
                  
                  </div>
                 </div>
                
                <div class="col-md-12 fitness_content">
                  {!! $calendar_details->calendar() !!}
                  {!! $calendar_details->script() !!}
                </div>
            </div>
    </div>
        </section>










        <!--================Start instragram Area =================-->
        <section class="instragram_area">
                <div class="instragram_left">
                    <div class="content">
                        <h2>Instagram</h2>
                        <p>Checkout what's happening in London</p>
              </div>        
     
        <div class="sharethis-inline-follow-buttons"></div>
          
                </div>
                <div class="instragram_right">

                    <div class="intragram-gallary">

                            @foreach($instafeed as $insta)

                            @if($loop->iteration > 6)

                              @break

                             @endif

                             
                            <img  class="item" src="{{ $insta->images->standard_resolution->url }}" />


                            @endforeach



                    </div>
                </div>
            </section>
            <!--================End instragram Area =================-->




@endsection

