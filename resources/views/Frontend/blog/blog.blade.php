@extends('layouts.frontend')

@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area">
        <h2>{{$article->title}}</h2>
        <a href="{{route('home')}}">Home /</a><a href="/culture/{{ $article->category }}">{{ $article->category }} /</a><span class="text-white">{{$article->title}}</span>
    </div>

        <!--================Blog tow column Area =================--> 
        <section class="blog_area blog_details">
            <div class="container">
                <div class="row blog_inner">
                   <!-- Blog Left Sidebar -->
                   <div class="col-lg-9 blog_lift_sidebar"> 
                        <!-- Blog items -->
                        <div class="blog_items"> 
                            <a href="#" class="blog_img"><img src="{{$article->image}}" alt=""></a>
                            <div class="breadcrumb_date">
                                <h6>{{ $article->created_at->format('jS F Y') }}</h6> 
                                  <ol class="breadcrumb"> 
                                    <li class="breadcrumb-item">Author : <a href="">{{$article->author}}</a></li>
                                    <li class="breadcrumb-item">Category : <a href="/culture/{{ $article->category }}">{{ $article->category }}</a></li>
                       
                                </ol>  
                            </div>
                            <a href="#" class="blog_tittle">{{$article->title}}</a>
                            {!!$article->body!!}
                        </div> 
                       
                   
                   </div> 
                   
                    <!-- Blog Right Sidebar -->
                    <div class="col-lg-3 blog_right_sidebar">  
                       
                        <aside class="r_widget categories_widget"> 
                            <h3>Categories</h3> 
                            <ul>
                              @foreach ($categories as $category )
                                 <li><a href="/culture/{{ $category }}">{{ $category }}<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                              @endforeach
                          </ul>
                        </aside>
                       
                        <aside class="r_widget recent_widget"> 
                            <h3>Recent News</h3> 
                            <div class="recent_inner">
                                @foreach ($articles as $article )
                                
                                <div class="media">
                                    <a href="#"><img src="{{$article->image}}" alt="{{$article->title}}"></a>
                                    <div class="media-body">
                                        <a href="/culture/article/{{ $article->id }}/{{ str_slug($article->title)}}">{{$article->title}}</a>
                                        <h6>{{ $article->created_at->format('jS F Y') }}</h6>
                                        <h6><a href="/culture/{{ $article->category }}">{{$article->category }}</a></h6>
                                    </div>
                                </div> 
                                @endforeach
                          

                            </div>
                        </aside>
                      
                   </div>
                </div>
            </div>
        </section>
        <!--================End Blog tow column Area =================--> 

@endsection

