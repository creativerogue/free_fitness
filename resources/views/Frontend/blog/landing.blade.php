@extends('layouts.frontend')

@section('content')
<style>
.banner_area {
    background: url({{$header}}) no-repeat scroll center center !important;
}
</style>
<div class="banner_area">
     @if ($category)

         <h2> {{$category}}</h2>
  
     @else  
         
         <h2>Fitness Culture</h2>

     @endif

         <a href="{{route('home')}}">Home </a>   
       
        @if ($category)

               
         <span class="text-white">{{$category}}</span>
  
         @else 

<span class="text-white">Fitness Culture</span>
     @endif
    </div>
  <!--================Blog tow column Area =================--> 
        <section class="blog_area">
            <div class="container">
                <div class="row blog_inner">
                   <!-- Blog Left Sidebar -->
                   <div class="blog_without"> 
                        <!-- Blog items -->

                        @foreach ($articles as $article )
                            
                                                
                        <div class="blog_items"> 
                            <a href="/culture/article/{{ $article->id }}/{{ str_slug($article->title)}}" class="blog_img"><img src="{{$article->image}}" alt="{{$article->title}}"></a>
                            <a href="/culture/article/{{ $article->id }}/{{ str_slug($article->title)}}" class="blog_tittle">{{$article->title}}</a>
                            <div class="breadcrumb_date">
                                <h6>{{ $article->created_at->format('jS F Y') }}</h6> 
                                    
                               
                                 <ol class="breadcrumb"> 
                                    <li class="breadcrumb-item">Author : <a href="">{{$article->author}}</a></li>
                                    <li class="breadcrumb-item">Category : <a href="/culture/{{ $article->category }}">{{ $article->category }}</a></li>
                       
                                </ol>  
                          
                          
                            </div>
                           
                            
                            
                            
                        </div> 
                        <!-- Blog items -->
                       @endforeach
                        
                        <!-- Pagination 
                        <div class="col next_pages">
                            <ul class="pagination justify-content-center">
                                <li><a href="#" class="active">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>-->
                   </div>  
                </div>
            </div>
        </section>
        <!--================End Blog tow column Area =================--> 


@endsection

