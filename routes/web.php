<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@home')->name('home');

Route::get('/culture', 'PagesController@blogLanding')->name('culture');
Route::get('/culture/article/{id}/{slug?}', 'PagesController@blogItem')->name('article');
Route::get('/culture/{category}', 'PagesController@blogcatagory')->name('category');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/clubs', 'PagesController@clubs')->name('clubs');
Route::get('/club/{id}/{slug?}', 'PagesController@club')->name('club');
//Route::get('/contact', 'PagesController@contact')->name('contact');

Route::get('/search', 'SearchController@Search');


Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('dashboard')->middleware('auth');;
Route::resource('dashboard/category','CategoryController')->middleware('auth');
Route::resource('dashboard/clubs', 'ClubController')->middleware('auth');
Route::resource('dashboard/events', 'EventController')->middleware('auth');
Route::resource('dashboard/users', 'UserController')->middleware('auth');
Route::resource('dashboard/locations', 'LocationController')->middleware('auth');
Route::resource('dashboard/articles', 'ArticleController')->middleware('auth');
Route::resource('dashboard/content', 'ContentController')->middleware('auth');


